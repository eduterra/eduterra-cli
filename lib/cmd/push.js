'use strict';

var Client = require('../client')
  , utils = require('../utils')
  , async = require('async')
  , path = require('path')
  , fs = require('fs')
  , colors = require('cli-color')
  , done = require('../done');

module.exports = exports = function (opts) {
  var dir = process.cwd()
    , siteRoot = path.join(dir, 'site')
    , force = opts.force
    , mtimes = {}
    , cSuccess = 0
    , cFailed = 0
    , cSkipped = 0;

  Client.getClient(function (err, cli) {
    if (err) return done(err);

    function _push(tree, cb) {
      // Process directories recursively
      if (tree.files)
        return async.eachSeries(tree.files, _push, cb);
      // Process single file
      var file = path.join(siteRoot, tree.relPath);
      // Check if file can be skipped
      var skip = !force && mtimes[tree.relPath] &&
        mtimes[tree.relPath] >= tree.mtime;
      if (skip) {
        cSkipped += 1;
        return cb();
      }
      // Push to EduTerra
      var request = cli.request('post', '/site/file/' + tree.relPath);
      var r = request({}, function (err, res) {
        if (err) return cb(err);
        if (res.statusCode != 200) {
          // Report failure and skip
          console.error('%s %s %s',
            colors.red('✘'),
            tree.relPath,
            colors.blackBright(res.statusCode));
          cFailed += 1;
          return cb();
        }
        cSuccess += 1;
        console.log('%s %s', colors.green('✔'), tree.relPath);
        cb();
      });
      var form = r.form();
      form.append('file', fs.createReadStream(file));
    }

    function _processMtimes(tree) {
      if (tree.files)
        return tree.files.forEach(_processMtimes);
      mtimes[tree.relPath] = tree.mtime;
    }

    cli.get('/site', {}, function (err, res, body) {
      if (err) return done(err);
      if (res.statusCode != 200)
        return done(new Error(body.error));

      if (!force)
        _processMtimes(body);

      utils.ls(siteRoot, function (err, tree) {
        if (err) return done(err);
        _push(tree, function (err) {
          if (err) return done(err);
          console.log('Push done:');
          console.log('\t%s file(s) pushed', colors.green(cSuccess));
          console.log('\t%s errors', colors.red(cFailed));
          console.log('\t%s files skipped', colors.blackBright(cSkipped));
          done();
        });
      });

    });

  });
};
