'use strict';

var Client = require('../client')
  , async = require('async')
  , path = require('path')
  , fs = require('fs')
  , mkdirp = require('mkdirp')
  , colors = require('cli-color')
  , done = require('../done');

module.exports = exports = function (opts) {
  var dir = process.cwd()
    , force = opts.force
    , cSuccess = 0
    , cFailed = 0
    , cSkipped = 0;

  Client.getClient(function (err, cli) {
    if (err) return done(err);

    function _request(tree, file, cb) {
      // Pull the file from EduTerra
      var request = cli.request('get', 'site/file/' + tree.relPath);
      request({ encoding: null }, function (err, res, body) {
        if (err) return cb(err);
        if (res.statusCode != 200) {
          // Report failure and skip
          console.error('%s %s %s',
            colors.red('✘'),
            tree.relPath,
            colors.blackBright(res.statusCode));
          cFailed += 1;
          return cb();
        }
        fs.writeFile(file, body, function (err) {
          if (err) return cb(err);
          console.log('%s %s', colors.green('✔'), tree.relPath);
          cSuccess += 1;
          // Subtract 1 second from mtime to be sure it wont be downloaded one more time
          fs.utimes(file, new Date(), new Date(tree.mtime - 1000), cb);
        });
      });
    }

    function _pull(tree, cb) {
      // Process directories recursively
      if (tree.files)
        return async.eachSeries(tree.files, _pull, cb);
      // Process single file
      var file = path.join(dir, 'site', tree.relPath)
        , parent = path.dirname(file);
      mkdirp(parent, function (err) {
        if (err) return cb(err);
        // Check if file can be skipped
        fs.stat(file, function (ignoredErr, stat) {
          var skip = !force && tree.mtime && stat &&
            stat.mtime.getTime() < tree.mtime;
          if (skip) {
            cSkipped += 1;
            return cb();
          }
          _request(tree, file, cb);
        });
      });
    }

    cli.get('/site', {}, function (err, res, body) {
      if (err) return done(err);
      if (res.statusCode != 200)
        return done(new Error(body.error));
      _pull(body, function (err) {
        if (err) return done(err);
        console.log('Pull done:');
        console.log('\t%s file(s) pulled', colors.green(cSuccess));
        console.log('\t%s errors', colors.red(cFailed));
        console.log('\t%s files skipped', colors.blackBright(cSkipped));
        done();
      });
    });

  });
};
