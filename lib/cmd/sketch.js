'use strict';

var fs = require('fs-extra')
  , path = require('path')
  , jade = require('jade')
  , express = require('express')
  , sprintf = require('sprintf')
  , rho = require('rho')
  , stylus = require('stylus')
  , _ = require('underscore');

var renderJade = function (res, file, data, cb) {
  var dir = path.dirname(file);
  fs.readFile(file, 'utf-8', function (err, text) {
    if (err) return cb(err);
    try {
      var fn = jade.compile(text, {
        filename: file,
        basedir: '/site',
        cache: false,
        compileDebug: false
      });
      var opts = _.extend({
        rho: function (file) {
          try {
            var text = fs.readFileSync(path.join(dir, file), 'utf-8');
            return rho.toHtml(text);
          } catch (e) {
            return sprintf('<p class="error">File %s not found.</p>', file);
          }
        },
        staticUrl: '/static'
      }, data);
      cb(null, fn(opts));
    } catch (e) {
      res.render('site/jadeError', {
        error: e,
        file: file,
        fullPath: file
      }, cb);
    }
  });
};

module.exports = exports = function (opts) {
  var dir = process.cwd();
  var port = opts.port || 3000;

  fs.readJson(path.join(dir, 'eduterra.json'), 'utf-8', function (err, realm) {
    if (err) {
      console.error('eduterra.json not found.');
      process.exit(1);
    }

    var app = express();
    app.set('views', path.join(dir, '/site'));
    app.set('view engine', 'jade');

    app.use('*', function (req, res, next) {
      res.locals.staging = true;
      res.locals.staticUrl = '/static';
      // TODO add all locals
      next();
    });

    app.get('/', function (req, res, next) {
      req.url = '/index.html';
      next();
    });

    app.use('/sections/:section/*', function (req, res, next) {
      var id = req.params.section;
      var section = {
        id: id,
        title: id,
        dir: dir + '/site/sections/' + id,
        pages: []
      };
      fs.readJson(path.join(section.dir, 'section.json'), 'utf-8', function (ignoredErr, obj) {
        if (obj)
          _.extend(section, obj);
        res.locals.section = section;
        res.locals.sectionUrl = '/sections/' + section.id;
        next();
      });
    });

    app.get('/sections/:section/:page.html', function (req, res) {
      var section = res.locals.section
        , sectionDir = dir + '/site/sections/' + section.id
        , pageFile = path.join(sectionDir, req.params.page + '.rho');
      fs.readFile(pageFile, 'utf-8', function (err, text) {
        if (err) return res.sendStatus(404);
        var html = rho.toHtml(text);
        var data = {
          page: req.params.page,
          html: html,
          section: section
        };
        // Try custom layout
        var layout = sectionDir + '/_layout.jade';
        renderJade(res, layout, data, function (err, html) {
          if (err) res.render('section/page', data);
          else res.send(html);
        });
      });
    });

    app.get('/*.html', function (req, res) {
      res.render(req.params[0]);
    });

    app.use('/static/css', stylus.middleware({
      src: path.join(dir, './site/static/css'),
      dest: path.join(dir, './site/static/css'),
      compile: function (str, path) {
        return require('stylus')(str)
          .set('filename', path)
          .set('compress', true)
          .set('include css', true)
          .use(require('nib')())
          .import('nib');
      }
    }));

    app.use('/static', express.static(path.join(dir, './site/static')));

    app.all('*', function (req, res) {
      res.redirect('http://' + realm.host + req.url);
    });

    app.listen(port, function () {
      console.log('Sketch is available at http://localhost:%s', port);
    });

  });
};
