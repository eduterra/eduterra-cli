'use strict';

var fs = require('fs')
  , path = require('path')
  , utils = require('../utils')
  , done = require('../done');

module.exports = exports = function () {
  var dir = process.cwd();
  // Ensure empty directory
  fs.readdir(dir, function (err, files) {
    if (err) return done(err);
    if (files.indexOf('eduterra.json') > -1)
      return done(new Error('eduterra.json already exists. Exiting.'));
    // Prompt for data
    var prompt = require('prompt');
    prompt.message = 'init';
    prompt.start();
    prompt.get([
      {
        name: 'id',
        description: 'Realm ID',
        required: true
      },
      {
        name: 'host',
        description: 'Realm Host',
        required: true
      },
      {
        name: 'publicKey',
        description: 'Public Key',
        required: true
      },
      {
        name: 'privateKey',
        description: 'Private Key (input is hidden)',
        required: true,
        hidden: true
      }
    ], function (err, results) {
      if (err) return done(err);
      results.host = results.host.replace(/^.*:\/\//, '').replace(/\/.*/g, '');
      var templates = path.join(__dirname, '../../template');
      utils.copy(templates, dir, results, done);
    });
  });

};
