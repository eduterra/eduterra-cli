'use strict';

var Client = require('../client')
  , utils = require('../utils')
  , colors = require('cli-color')
  , done = require('../done');

module.exports = exports = function () {
  Client.getClient(function (err, cli) {
    if (err) return done(err);
    var msg = utils.randomString(16);
    cli.get('/echo', { message: msg }, function (err, res, body) {
      if (err) return done(err);
      if (res.statusCode == 200 && msg == body.echo)
        console.log('%s It works!', colors.green('✔'));
      else
        console.error('%s %s — %s', colors.red('✘'), res.statusCode, body.error);
      done();
    });
  });
};
